<?php
/**
 * @file
 * rules hooks
 */


/**
 * Implements hook_rules_event_info().
 *
 * This function declares all the possible workflow transition events.
 */
function wf_field_rules_event_info() {
  // in the core module we're only implementing for nodes
  module_load_include('inc', 'wf_field');
  $events = _wf_field_build_entity_transition_rules('node', 'wf_field', array(
    'node' => array('type' => 'node', 'label' => t('updated content')),
    'account' => array('type' => 'user', 'label' => t('acting user')),
    'author' => array('type' => 'user', 'label' => t('node author')),
  ));
  return $events;
}

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all possible workflow transition actions
 */
function wf_field_rules_action_info() {
  // we're only implementing actions for the node entity-type
  return array(
    'wf_field_set_state' => array(
      'label' => t('Set state of a node of a wf_field'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
        'field_name' => array(
          'type' => 'text',
          'options list' => 'wf_field_rules_field_options',
          'label' => t('wf_field instance to compare'),
                  'optional' => FALSE,
          'restriction' => 'input',
        ),
        'field_value' => array(
            'type' => 'text',
            'label' => t('Workflow Status'),
          'options list' => 'wf_field_rules_field_value_options',
        ),
      ),
      'group' => t('Workflow Field'),
      'base' => 'wf_field_rules_action_set_state',
      'callbacks' => array(),
    ),
  );
}

function wf_field_rules_action_set_state($node, $field_name, $state, $element=NULL) {
  $node_wf_wrapped = wf_field_entity_wrapper( 'node', $node );
  // check field_names on this node
  $fns = $node_wf_wrapped->getFieldNames();
  // check that we have a field by this name
  if ( in_array($field_name, $fns) ) {
    // @todo: maybe log failures?
    $node_wf_wrapped->getFieldPropertyWrapper($field_name)->transition($state);
  }
  else {
    // @todo: maybe log?
    return NULL;
  }
}


/**
 * Form alter callback for the action wf_field_set_state.
 * Use multiple steps to configure the condition as the needed type of the value
 * depends on the selected data.
 */
function wf_field_rules_action_set_state_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  // reuse
  return wf_field_condition_state_is_form_alter($form, $form_state, $options, $element);
}


/**
 * Implements hook_rules_condition_info().
 *
 * This function declares all possible workflow transition actions
 */
function wf_field_rules_condition_info() {
  return array(
    'wf_field_state_is' => array(
      'label' => t('wf_field state comparison'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
        'field_name' => array(
          'type' => 'text',
          'options list' => 'wf_field_rules_field_options',
          'label' => t('wf_field instance to compare'),
                  'optional' => FALSE,
          'restriction' => 'input',
        ),
        'field_value' => array(
          'type' => 'list<text>',
          'label' => t('Workflow Status'),
          'description' => t('Select one state or multiple states that the object should be in'),
          'options list' => 'wf_field_rules_field_value_options',
          'optional' => FALSE,
            'restriction' => 'input',
        ),
      ),
      'group' => t('Workflow Field'),
      'base' => 'wf_field_condition_state_is',
    ),
  );
}


/**
 * Form alter callback for the condition wf_field_state_is.
 * Use multiple steps to configure the condition as the needed type of the value
 * depends on the selected data.
 */
function wf_field_condition_state_is_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  // @todo: refactor this to play nicely with all kinds of entities?
  if (!empty($options['init']) && !isset($form_state['rules_element_step'])) {
    unset($form['parameter']['field_value']);
    $form['negate']['#access'] = FALSE;
    $form_state['rules_element_step'] = 'field_value';
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#limit_validation_errors' => array(array('parameter', 'field_name')),
      '#submit' => array('rules_form_submit_rebuild'),
    );
    // Clear the parameter mode for the value parameter, so its gets the proper
    // default value based upon the type of the the selected data on rebuild.
    unset($form_state['parameter_mode']['field_value']);
  }
  else {
    // Change the data parameter to be not editable.
    $form['parameter']['field_name']['settings']['#access'] = FALSE;
    // TODO: improve display
    $form['parameter']['field_name']['info'] = array(
      '#prefix' => '<p>',
      '#markup' => t('<strong>Selected field:</strong> %selector', array('%selector' => $element->settings['field_name'])),
      '#suffix' => '</p>',
    );

  }
}

/**
 * Provides the content type of a node as asserted metadata.
 * @todo: i have not figured out where this shows up (i assume in the data selector somewhere)
 */
function wf_field_condition_state_is_assertions($element) {
  return array('wf_field' => array('field_name' => $element->settings['field_name'], 'field_value' => $element->settings['field_value']));
}


/**
 * Check whether a node meets the conditions
 * @param stdClass $node
 * @param string $field_name
 * @param array $states
 * @param RulesAbstractPlugin $element
 * @return  mixed   NULL on node not have a wf_field-field instance of the name $field_name; bool otherwise
 */
function wf_field_condition_state_is($node, $field_name, $states=array(), $element=NULL) {
  $node_wf_wrapped = wf_field_entity_wrapper( 'node', $node );
  // field_names on this
  $fns = $node_wf_wrapped->getFieldNames();
  // check that we have a field by this name
  if ( in_array($field_name, $fns) ) {
    $state = $node_wf_wrapped->getFieldPropertyWrapper($field_name)->raw();
    return in_array( $state, $states );
  }
  else {
    return NULL;
  }
}

/**
 * List all wf_fields for the node entity type
 * @todo: make this work w/ all entities?
 * @return    array
 */
function wf_field_rules_field_options() {
  module_load_include('inc', 'wf_field');
  $instances = _wf_field_get_all_instances('node');
  $options = array();
  foreach ($instances as $type => $info) {
    $options[ $info['field_name'] ] = t('@label (@entity_type:@key)', array('@label' => $info['label'], '@entity_type' => $info['entity_type'], '@key' => $info['field_name']));
  }
  return $options;
}

/**
 * Returns states dependent on wf_field field selected
 * @param RulesAbstractPlugin $element
 * @return array  keyed state_id => state_label
 */
function wf_field_rules_field_value_options($element) {
  $field_name = $element->settings['field_name'];
  module_load_include('inc', 'wf_field');
  $states = _wf_field_get_states($field_name);
  return $states;
}