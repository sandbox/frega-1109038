<?php
/**
 * @file
 * Entity info hooks file.
 */

/**
 * Callback returning the options list of a field.
 */
function wf_field_property_options_list($name, $info) {
  return entity_metadata_field_options_list($name, $info);
}