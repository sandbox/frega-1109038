wf_field - bare-bones workflow functionality as field for Drupal 7.

wf_field is a simplistic workflows implementation reliying on the rules module and field APIs to do the heavy lifting. 

wf_field was inspired by the Drupal 6 "Rules + CCK for Publication Workflow"-tutorial (http://drupal.org/node/550716) and johan falk's (@itangalo) proof-of-concept screencast (http://nodeone.se/blogg/workflow-field-proof-of-concept) as well as the discussion on g.d.o (http://groups.drupal.org/node/73153#comment-370844).

The module provides a very basic UI for workflow modelling within the settings form of the field UI (allowing users to define, name (and "disable") transitions). This architecture allows wf_field to be used in any fieldable entity (some glue-code still required, see below).

The workflow logic is handled by the the rules.module for which wf_field exposes the required reactions, actions and conditions.

Features:
- Fields: modelling in the field-settings form, field formatters
- Rules integration: reactions, actions, conditions
- Views integration: field handler (for exposed filters)
- Entity API integration: exposing wf_field fields to the entity api (WIP)
- Permissions: permissions for every transition defined
- DX: OO-oriented "wrapper" classes (wrapping individual nodes and fields)

Submodules:
- wf_field_access_override: Create custom permissions settings to override view,edit, delete permissions on a per-workflow-state basis
- wf_field_nodelog: Log workflow transitions for nodes (entity, views, rules integration)
- wf_field_simplelog: Debugging module to log any transition to watchdog
- wf_field_simpleaccess: Basic access module, revoking node edit-permissions for any user without the ability to transition the node to a different workflow state.

- wf_field_taxonomy, wf_field_user: Initial stabs at making other core entities wf_field-fieldable.

