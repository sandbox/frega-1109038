<?php
/**
 * @file
 *
 * Administrative page callbacks.
 */
function wf_field_nodelog_admin_settings() {
  module_load_include('inc', 'wf_field');
  $fields = _wf_field_get_all_fields('node');

  $values = array();
  foreach ($fields as $key => $field_info) {
    $values[$key] = $key . ' (' . implode(", ", $field_info['bundles']['node']) . ')';
  }

  $default_value = variable_get('wf_field_nodelog_log_instance_names', NULL);
  $default_value = ($default_value===NULL) ? array_keys($values) : $default_value;

  $form['wf_field_nodelog_log_instance_names'] = array(
    '#title' => t('Keep transition log for these workflow fields:'),
    '#description' => t('Every transition will be logged.'),
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#options' => $values,
    '#default_value' => $default_value,
  );

  // Log all saves?
  $default_value = variable_get('wf_field_nodelog_log_non_transition_updates_instance_names', NULL);
  $default_value = ($default_value===NULL) ? array_keys($values) : $default_value;
  $form['wf_field_nodelog_log_non_transition_updates_instance_names'] = array(
    '#title' => t('Keep an extended log for these workflow fields (including non-transition saves):'),
    '#description' => t('Every node update will be logged, i.e. <strong>also when no transition takes place</strong>.'),
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#options' => $values,
    '#default_value' => $default_value,
  );
  return system_settings_form($form);
}