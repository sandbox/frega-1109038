<?php
/**
 * @file
 * Rules hooks file.
 */

/**
 * Implements hook_rules_condition_info().
 *
 * This function declares all possible workflow transition actions
 */
function wf_field_nodelog_rules_condition_info() {
  return array(
    'wf_field_nodelog_state_has_been' => array(
      'label' => t('wf_field_nodelog state comparison (has been in a state)'),
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Content')),
        'field_name' => array(
          'type' => 'text',
          'options list' => 'wf_field_nodelog_rules_field_options',
          'label' => t('wf_field_nodelog instance to compare'),
          'optional' => FALSE,
          'restriction' => 'input',
        ),
        'field_value' => array(
          'type' => 'list<text>',
          'label' => t('Workflow Status'),
          'description' => t('Select one state or multiple states that the object should be in'),
          'options list' => 'wf_field_nodelog_rules_field_value_options',
          'optional' => FALSE,
          'restriction' => 'input',
        ),
      ),
      'group' => t('Workflow Field'),
      'base' => 'wf_field_nodelog_condition_state_has_been',
    ),
  );
}


/**
 * Form alter callback for the condition wf_field_nodelog_state_is.
 * Use multiple steps to configure the condition as the needed type of the value
 * depends on the selected data.
 */
function wf_field_nodelog_condition_state_has_been_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  module_load_include('rules.inc', 'wf_field');
  wf_field_condition_state_is_form_alter($form, $form_state, $options, $element);
}

/**
 * Provides the content type of a node as asserted metadata.
 * @todo: i have not figured out where this shows up (i assume in the data selector somewhere)
 */
function wf_field_nodelog_condition_state_has_been_assertions($element) {
  return array('wf_field_nodelog' => array('field_name' => $element->settings['field_name'], 'field_value' => $element->settings['field_value']));
}


/**
 * Check whether a node meets the conditions
 * @param stdClass $node
 * @param string $field_name
 * @param array $states
 * @param RulesAbstractPlugin $element
 * @return  mixed   NULL on node not have a wf_field_nodelog-field instance of the name $field_name; bool otherwise
 */
function wf_field_nodelog_condition_state_has_been($node, $field_name, $states=array(), $element=NULL) {
  $node_wf_wrapped = wf_field_entity_wrapper( 'node', $node );
  // field_names on this
  $fns = $node_wf_wrapped->getFieldNames();
  // check that we have a field by this name
  if ( in_array($field_name, $fns) ) {
    // build a query for the historic log querying to_state and from_state
    // @todo: allow filter "has transitioned to" and "has transitioned from"
    // @todo: do not use db_select()
    $lid = db_select('wf_field_nodelog', 'wf_nl')
      ->fields('wf_nl', array('lid', 'entity_id', 'to_state', 'from_state'))
      ->condition('entity_id', $node->nid, '=')
      ->condition( db_or()->condition('to_state',  $states, 'IN', 'OR' )->condition('from_state',  $states, 'IN', 'OR' ) )
      ->execute()
      ->fetchField();
    return (bool) $lid;
  }
  else {
    return NULL;
  }
}

/**
 * List all wf_fields for the node entity type
 * @todo: make this work w/ all entities?
 * @return  array
 */
function wf_field_nodelog_rules_field_options() {
  module_load_include('rules.inc', 'wf_field');
  return wf_field_rules_field_options();
}

/**
 * Returns states dependent on wf_field field selected
 * @param RulesAbstractPlugin $element
 * @return array  keyed state_id => state_label
 */
function wf_field_nodelog_rules_field_value_options($element) {
  module_load_include('rules.inc', 'wf_field');
  return wf_field_rules_field_value_options($element);
}
