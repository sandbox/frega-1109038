<?php
/**
 * @file
 * Views hooks file.
 */

/**
 * Controller for generating views for wf_field_simplelog
 */
class WfFieldNodeLogViewsController extends EntityDefaultViewsController {

  protected $type, $info, $relationships;

  public function __construct($type) {
    $this->type = $type;
    $this->info = entity_get_info($type);
  }

  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    $base_table = $this->info['base table'];
    // merge in the entity relationships
    $data[$base_table]['entity_id']['relationship'] = array(
      'base' => 'node',
      'base field' => 'nid',
      'field' => 'entity_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Node relation'),
    );
    $data[$base_table]['uid']['relationship'] = array(
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Acting user relation'),
    );

    // inverse relations
    $data[$base_table]['table']['join'] = array(
      // Index this array by the table name to which this table refers.
      // 'left_field' is the primary key in the referenced table.
      // 'field' is the foreign key in this table.
      'node' => array(
        'left_field' => 'nid',
        'field' => 'entity_id',
      ),
      'users' => array(
        'left_field' => 'uid',
        'field' => 'uid',
      ),
    );

    // rewrite the mapping - unix epoch for timestamp
    $data[$base_table]['timestamp']['field']['handler'] = 'views_handler_field_date';

    // $data[$base_table]['timestamp']['field']['handler'] = 'views_handler_field_date';


    return $data;
  }

}