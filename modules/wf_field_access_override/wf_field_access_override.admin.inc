<?php
/**
 * @file
 *
 * Administrative page callbacks.
 */
function wf_field_access_override_admin_settings() {
  module_load_include('inc', 'wf_field');
  $fields = _wf_field_get_all_fields('node');

  $form['#prefix'] =
    t(
      'Please select for which workflow fields, workflow states and workflow actions you need to be able to deny access.<br/>' .
      'Note: these permissions need to be configured on the !permissions_link and these permissions do affect individual nodes ONLY.',
        array('!permissions_link' => l( t('permissions page'), 'admin/people/permissions', array('fragment' => 'module-wf_field_access_override')))
    );

  $options = array();
  foreach ($fields as $key => $field_info) {
    //
    $node_bundles = $field_info['bundles']['node'];
    foreach ($node_bundles as $i => $bundle_name) {
      if (!isset($options[ $bundle_name ])) {
        $options[ $bundle_name ] = array();
      }
      $options[ $bundle_name ][] = $field_info;
    }
  }

  $ps = _wf_field_access_override_permissions_options();

  $df = variable_get('wf_field_access_override_node_bundle_' . $bundle_name , array() );
  $keys = array();

  foreach ($options as $bundle_name => $field_infos ) {
    $form['wf_field_access_override_node_bundle_' . $bundle_name ] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Content Type (node bundle): %bundle_name', array('%bundle_name' => $bundle_name)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    // keep track of the keys for easier validation / submission
    $keys[] = 'wf_field_access_override_node_bundle_' . $bundle_name;

    $selected_bundle = FALSE;
    foreach ($field_infos as $field_info) {
      $fn = $field_info['field_name'];
      $states = _wf_field_get_states($fn);
      $form['wf_field_access_override_node_bundle_' . $bundle_name ][$fn] = array(
        '#title' => t('Workflow Field %field_name', array('%field_name' => $fn)),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#tree' => TRUE
      );
      $selected = FALSE;
      foreach ( $states as $key => $state_label ) {
        $dfv = isset($df[$fn][$key]) ? $df[$fn][$key] : array();
        $form['wf_field_access_override_node_bundle_' . $bundle_name ][$fn][$key] = array(
          '#title' => t('State %state_label', array('%state_label' => $state_label)),
          '#description' => t('Please check the permissions you want to expose for the field %field_name in the state %state_label', array('%state_label' => $state_label, '%field_name' => $fn)),
          '#type' => 'checkboxes',
          '#multiple' => TRUE,
          '#options' => $ps,
          '#default_value' => $dfv
        );
        $selected = $selected || sizeof($dfv);
      }
      // open the fieldset if something has been selected
      if ($selected) {
        $form['wf_field_access_override_node_bundle_' . $bundle_name ][$fn]['#collapsed'] = FALSE;
        $selected_bundle = TRUE;
      }
    }
    // open the fieldset if something has been selected
    if ($selected_bundle) {
      $form['wf_field_access_override_node_bundle_' . $bundle_name ]['#collapsed'] = FALSE;
    }
  }
  // pass-on for easier submission/validation
  $form['wf_field_access_override_keys'] = array('#type' => 'value', '#value' => $keys);

  $form['#submit'][] = 'wf_field_access_override_admin_settings_submit';
  return system_settings_form($form);
}

function wf_field_access_override_admin_settings_submit($form, &$form_state) {
  // @todo: warn if we remove permissions that have been set ...
  $values = $form_state['values'];
  foreach ($values['wf_field_access_override_keys'] as $key ) {
    $field_info = $values[$key];
    foreach ( $field_info as $field_name => $settings ) {
      foreach ($settings as $state_key => $info)  {
        foreach ($info as $op_key => $v) {
          if (isset($v) && !$v) {
            unset($form_state['values'][$key][$field_name][$state_key][$op_key]);
          }
        }
        // remove this section
        if (!sizeof($form_state['values'][$key][$field_name][$state_key])) {
          unset($form_state['values'][$key][$field_name][$state_key]);
        }
      }
      // remove this section
      if (!sizeof($form_state['values'][$key][$field_name])) {
        unset($form_state['values'][$key][$field_name]);
      }
    }
  }

  drupal_set_message(
    t('Please note: you will need to configure the actual permissions on the !permissions_link.',
    array('!permissions_link' => l( t('permissions page'), 'admin/people/permissions', array('fragment' => 'module-wf_field_access_override')))), 'warning');

  drupal_set_message(
    t('Please also note: it is advised to revoke permissions before "removing" them here; otherwise you can end up with strange effects or some permissions continuing to be active and others being ignored!'),
    'warning'
  );
}

function _wf_field_access_override_permissions_options() {
  return array(
    'deny view' => t('DENY View others\' content'),
    'deny update' => t('DENY Edit others\' content'),
    'deny delete' => t('DENY Delete others\' content'),
    'deny view own' => t('DENY View own content'),
    'deny update own' => t('DENY Edit own content'),
    'deny delete own' => t('DENY Delete own content'),
    'allow view' => t('ALLOW View others\' content'),
    'allow update' => t('ALLOW Edit others\' content'),
    'allow delete' => t('ALLOW Delete others\' content'),
    'allow view own' => t('ALLOW View own content'),
    'allow update own' => t('ALLOW Edit own content'),
    'allow delete own' => t('ALLOW Delete own content') );
}