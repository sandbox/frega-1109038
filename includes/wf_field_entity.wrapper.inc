<?php
/**
 * @file
 * Decorator class for entities to improve/ease developer experience
 *
 * @author frega
 */
class WfFieldEntityWrapper {
  /**
   * Entity object this object is wrapped around
   * @var stdClass
   */
  private $entity;

  /**
   * Entity object this object is wrapped around
   * @var stdClass
   */
  private $entity_type;

  /**
   * Entity object this object is wrapped around
   * @var stdClass
   */
  private $bundle_name;

  /**
   * Entity.module wrapped entity
   * @var stdClass
   */
  private $wrapped_entity;

  /**
   * Active workflow field name
   * @var string
   */
  private $wf_field_field_name;

  /**
   * Default workflow field name
   * @var string
   */
  private $wf_field_default_field_name;

  /**
   * Array holding references to WfFieldPropertyWrapper
   * @var Array<WfFieldPropertyWrapper>
   */
  private $field_property_wrappers = array();

  /**
   * Options passed
   * @var  array
   */
  private $options=array();

  /**
   * Sets the entity this object wraps
   * @param  object $entity
   * @return   wf_field_entity_wrapper
   */
  private function _setEntity( $entity, $options=array() ) {
    $this->entity = $entity;
    // @todo: hanlde
    if ( $this->entity_type == 'node' ) {
      $this->bundle_name = $entity->type;
    }
    return $this;
  }

  /**
   * Retrieve the wrapped entity (entity.module)
   * @return EntityMetadataWrapper
   */
  private function getWrappedEntity() {
    if (!isset($this->_wrapped_entity)) {
      $this->wrapped_entity = entity_metadata_wrapper($this->entity_type, $this->entity);
    }
    return $this->wrapped_entity;
  }

  /**
   * Constructor
   * @param    string   $entity_type
   * @param    object  $entity
   * @param    array    $options
   */
  function __construct( $entity_type, $entity, $options=array() ) {
    $this->entity_type = $entity_type;
    $this->_setEntity( $entity, $options );
    $this->options = $options;
  }

  /**
   * Does this entity instance have wf_field fields attached
   * @return  bool  true if it has at least one wf_field
   */
  function hasFields() {
    $names = $this->getFieldNames();
    return (bool) sizeof($names);
  }

  /**
   * Return the field instances for all wf_fields on this entity instance
   * @param string/NULL   optionally get a single field instance
   * @param array         either array of field instance-arrays or field instance array for $field_name-field
   */
  function getFieldInstances($field_name=NULL) {
    if (!isset($this->_field_instances)) {
      $this->_field_instances = _wf_field_get_all_instances($this->entity_type, $this->bundle_name);
    }
    if (isset($field_name)) {
      // @todo improve this - the array should be pre-keyed accordingly
      foreach ($this->_field_instances as $instance) {
        if ($instance['field_name'] == $field_name) {
          return $instance;
        }
      }
      return NULL;
    }
    else {
      return $this->_field_instances;
    }
  }

  /**
   * Return a field instance array by field name
   * @param string        field name or default/current wf_field
   * @param array         field instance array for $field_name-field
   */
  function getFieldInstance( $field_name=NULL ) {
    if (!isset($field_name)) {
      $field_name = $this->_getActiveFieldName();
    }
    return $this->getFieldInstances( $field_name );
  }

  /**
   * Returns a WfFieldPropertyWrapper instance for the
   * @param string/null $field_name or default
   * @return  WfFieldPropertyWrapper
   */
  function getFieldPropertyWrapper( $field_name=NULL ) {
    if (!isset($field_name)) {
      $field_name = $this->_getActiveFieldName();
    }
    if (!isset($this->field_property_wrappers[ $field_name] ) ) {
      // @todo: do we need to go through entity.module - for consistency sake that's better
      $this->field_property_wrappers[$field_name] = $this->getWrappedEntity()->{$field_name}->value();
    }
    return $this->field_property_wrappers[$field_name];
  }

  /**
   * Return field names of wf_field attached to this entity
   * @return array
   */
  function getFieldNames() {
    $names = array();
    foreach ( (array) $this->getFieldInstances() as $k => $v) {
      $names[] = $v['field_name'];
    }
    return $names;
  }

  /**
   * Return a WfFieldPropertyWrapper for the default/currently active wf-field
   * @todo: this ist not exactly
   * @param array $options
   * @return WfFieldPropertyWrapper
   */
  function value($options=array()) {
    return $this->getFieldPropertyWrapper()->value($options);
  }

  /**
   * Return a WfFieldPropertyWrapper for the default/currently active wf-field
   * @param unknown_type $options
   */
  function rawValue($options=array()) {
    return $this->getFieldPropertyWrapper()->getRawValue();
  }

  /**
   * Trigger a transition to a state
   * @param unknown_type $target_state_or_options
   * @return  bool
   */
  function transition($target_state, $options=array()) {
    return $this->getFieldPropertyWrapper()->transition($target_state, $options);
  }

  // {{{ DEFAULT FIELD AND SWITCHING FIELDS (PREPARATION FOR MULTIPLE FIELDS PER ENTITY)
  /**
   * Returns the active wf_field field name that we are operating on
   * @return  string  field name
   */
  private function _getActiveFieldName() {
    if (!isset($this->wf_field_field_name)) {
      if ( isset($this->wf_field_default_field_name) ) {
        $this->wf_field_field_name = $this->wf_field_default_field_name;
      }
      else {
        // get the first wf_field-field_name
        $names = $this->getFieldNames();
        $this->wf_field_field_name = $names[0];
      }
    }
    return $this->wf_field_field_name;
  }

  /**
   * Allow specifying to which field the actions apply
   * Because entities can have multiple wf_field-fields this allows to select one
   * @param    string    field name
   * @return  object  wf_field_wrapper
   */
  function withField($field_name) {
    $names = $this->getFieldNames();
    if (in_array($field_name, $names)) {
      $this->wf_field_field_name = $field_name;
    }
    else {
      throw new Exception('Invalid field name');
    }
    return $this;
  }

  /**
   * Uses the default (first) field
   * @return  object  wf_field_wrapper
   */
  function withDefaultField() {
    unset( $this->wf_field_field_name );
    $this->_getWfFieldName();
    return $this;
  }

  /**
   * Set a default field
   * @return  object  wf_field_wrapper
   */
  function setDefaultField($field_name) {
    $names = $this->getFieldNames();
    if (in_array($field_name, $names)) {
      $this->wf_field_default_field_name = $field_name;
    }
    else {
      throw new Exception('Invalid field name');
    }
    return $this;
  }

}