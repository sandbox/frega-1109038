<?php
/**
 * @file
 * Wrapper class for wf_field-property instances
 * Used to wrap property for entity.module and WfFieldEntityWrapper
 *
 * @author frega
 */
class WfFieldPropertyWrapper {

  /**
   * Constructor
   * Enter description here ...
   * @param stdClass $entity
   * @param array $options
   * @param string $name
   * @param string $entity_type
   * @param array $context
   */
  function __construct($entity, array $options, $name, $entity_type, &$context) {
    $this->entity = $entity;
    $this->entity_type = $entity_type;
    $this->options = $options;
    $this->name = $name;
    $this->context = $context;
  }


  /**
   * Magic toString conversion
   * @return int/null
   */
  function __toString() {
    return $this->getRawValue();
  }


  /**
   * Returns the field instance info
   * Enter description here ...
   */
  function getFieldInstanceInfo() {
    if (!isset($this->context['instance'])) {
      $this->context['instance'] = field_info_instance($this->context['parent']->type(), $this->name, $this->context['parent']->getBundle());
    }
    return $this->context['instance'];
  }

  /**
   * Returns the
   * Enter description here ...
   */
  function getFieldInfo() {
    if (!isset($this->context['field'])) {
      $this->context['field'] = field_info_field($this->name);
    }
    return $this->context['field'];
  }


  /**
   * Returns the (raw) scalar value
   */
  function getRawValue() {
    // Set contextual info useful for getters of any child properties.
    $instance = $this->getFieldInstanceInfo();
    $field_info = $this->getFieldInfo();
    $langcode = field_language($this->entity_type, $this->entity, $this->name, isset($this->options['language']) ? $this->options['language']->language : NULL);
    $name  = $this->name;
    return isset($this->entity->{$name}[$langcode][0]['wf_state']) ? $this->entity->{$name}[$langcode][0]['wf_state'] : NULL;
  }

  /**
   * Returns the current state label
   * @return  string
   */
  function getStateLabel( $options = array() ) {
    return wf_field_get_current_state_label($this->getRawValue(), $this->getFieldInstanceInfo() );
  }

  /**
   * Returns the current state label (passive)
   * @return  string
   */
  function getPassiveStateLabel() {
    $options = array();
    $options['passive'] = TRUE;
    return $this->getStateLabel($options);
  }

  /**
   * Alias for getRawValue()
   * @todo: is this sane?
   */
  function raw() {
    return $this->getRawValue();
  }

  /**
   * Alias for getStateLabel()
   * @todo: is this sane?
   */
  function value($options=array()) {
    return $this->getStateLabel($options);
  }

  /**
   * Trigger a transition
   * @param unknown_type $target_state_or_options
   */
  function transition($target_state, $options=array()) {
    return wf_field_transition_execute($this->entity, $this->name, $target_state, $options);
  }

  /**
   * Return available transitions (keyed by {transition_id})
   * @param array $options
   */
  function getAvailableTransitions($options=array() ) {
    if (!isset($options['account'])) {
      $options['account'] = $GLOBALS['user'];
    }
    return $this->getCurrentTransitionOptions($options);
  }

  /**
   * Return available transitions (keyed by target-{state_id})
   * @param array $options
   * @return array Target states array (keyed by target-{state_id})
   */
  function getAvailableTargetStates($options=array()) {
    // make sure we get the state ids back
    $options['index_by_state_id'] = TRUE;
    return $this->getAvailableTransitions($options);
  }

  /**
   * Return complete info on available transitions
   * @param array $options
   * @return array Transitions
   */
  function getAvailableTransitionsInfo($options=array()) {
    // make sure we get complete infos back
    $options['complete_info'] = TRUE;
    return $this->getAvailableTransitions($options);
  }

}
